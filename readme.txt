phpmyadmin for dockerized DBs

Usage:

1. Set DB containers names in PMA_HOSTS
2. Set absolute url to be served via NGINX in PMA_ABSOLUTE_URI
3. Create a password using openssl passwd and copy the result into 'pma_pass' file as username:copiedpassword
4. Set the url in VIRTUAL_HOST and LETSENCRYPT_HOST